package org.skendall.earlwatch.dir.test;

import org.apache.log4j.Logger;
import org.skendall.earlwatch.WatchListener;
import org.skendall.earlwatch.Watcher;
import org.skendall.earlwatch.WatcherEvent;
import org.skendall.earlwatch.WatcherFactory;
import org.skendall.earlwatch.WatcherFactory.WatcherType;

public class Bootstrap implements WatchListener {
	
	private static Logger logger = Logger.getLogger(Bootstrap.class.getName());
	
	public static void main(String args[]) {
		if(logger.isDebugEnabled())
			logger.debug("Starting bootstrap.");
		Watcher w = WatcherFactory.getInstance().newWatcher(WatcherType.FILE);
		if(logger.isDebugEnabled())
			logger.debug("Watcher configured for " + w);
		WatchListener l = new Bootstrap();
		if(logger.isDebugEnabled())
			logger.debug("Listener configured: " + l);
		w.addWatchListener("test",l);
		//w.addWatchListener("test/test.txt",l);
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void changed(WatcherEvent event) {
		logger.warn("[CHANGE] " + event);
	}
	
	public String toString() {
		return "Bootstrap";
	}
}
