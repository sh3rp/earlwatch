package org.skendall.earlwatch;

import java.util.List;

/**
 * Interface that describes the behavior of a Watcher object.  The watcher object
 * does most of the work in checking a URL, deciding if the URL and the backing
 * content has changed.  If the content has changed, a transition event occurs.
 * 
 * 
 * @author skendall
 *
 */


public interface Watcher {
	
	/**
	 * Provides a method to add a URL to watch and a listener to notify
	 * when that URL has changed.
	 * 
	 * @param url
	 * @param listener
	 */
	
	public void addWatchListener(String url, WatchListener listener);
	
	/**
	 * Provides a method to remove a specified URL and the listener attached
	 * to that URL and responsible for change notifications.
	 * 
	 * @param url
	 * @param listener
	 */
	
	public void removeWatchListener(String url, WatchListener listener);
	
	/**
	 * Provides a method to remove all listeners from a particular URL.
	 * 
	 * @param url
	 */
	
	public void removeAllListeners(String url);
	
	/**
	 * Provides a method to retrieve all transitions for a given URL.
	 * 
	 * @param url
	 * @return
	 */

	public List<Transition> getTransitions(String url);

}
