package org.skendall.earlwatch;

import difflib.Patch;

/**
 * A POJO that represents a transition and carries the last time
 * and difference information when compared to the last transition.
 * 
 * @author skendall
 *
 */

public class Transition {
	
	/**
	 * URL that made the transition
	 */
	
	private String url;
	
	/**
	 * Last modified time
	 */
	
	private long lastModified;
	
	/**
	 * Unified patch set for the transition
	 */
	
	private Patch diff;
	
	/**
	 * Retrieves the URL of the transition
	 * @return String
	 */
	
	public String getUrl() {
		return url;
	}
	
	/**
	 * Sets the URL of the transition
	 * @param url
	 */
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	/**
	 * Retrieves the last modified time (in epochs)
	 * @return
	 */
	
	public long getLastModified() {
		return lastModified;
	}
	
	/**
	 * Sets the last modified time (in epochs)
	 * @param lastModified
	 */
	
	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}
	
	/**
	 * Retrieves the unified diff for this transition
	 * @return
	 */
	
	public Patch getDiff() {
		return diff;
	}
	
	/**
	 * Sets the unified diff for this transition
	 * @param diff
	 */
	
	public void setDiff(Patch diff) {
		this.diff = diff;
	}
	
	public String toString() {
		return "[Transition] url=" + url + ", lastModified=" + lastModified;
	}
}
