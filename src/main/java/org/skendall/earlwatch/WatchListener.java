package org.skendall.earlwatch;

/**
 * The interface used by the Watcher to notify any parties of interest that
 * a change has occurred on the URL being watched.
 * 
 * @author skendall
 *
 */

public interface WatchListener {
	
	/**
	 * Called when a change event occurs within the watcher.
	 * 
	 * @param event
	 */
	
	public void changed(WatcherEvent event);
}
