package org.skendall.earlwatch;

import java.util.HashMap;
import java.util.Map;

import org.skendall.earlwatch.dir.FileWatcher;

/**
 * Generates a Watcher object based on the specified watcher type.
 * 
 * 
 * @author skendall
 *
 */

public class WatcherFactory {
	
	/**
	 * List of watcher types (this is probably hacky and could be better implemented
	 */
	
	public static Map<WatcherType,Class<? extends Watcher>> WATCHER_TYPES = new HashMap<WatcherType,Class<? extends Watcher>>();
	
	/**
	 * Types of watchers, used as a key in the watcher types mapping
	 * 
	 * @author skendall
	 *
	 */
	
	public enum WatcherType {
		FILE,HTTP;
	}
	
	/**
	 * More hackery, needs a better implementation
	 */
	
	static {
		WATCHER_TYPES.put(WatcherType.FILE, FileWatcher.class);
		WATCHER_TYPES.put(WatcherType.HTTP, null);
	}
	
	/**
	 * Singleton
	 */
	
	private static final WatcherFactory FACTORY = new WatcherFactory();
	
	private WatcherFactory() { }
	
	public static WatcherFactory getInstance() {
		return FACTORY;
	}
	
	/**
	 * End singleton
	 */
	
	/**
	 * Generates a new Watcher object.
	 * 
	 * @param type
	 * @return
	 */
	
	public Watcher newWatcher(WatcherType type) {
		Watcher watcher = null;
		
		Class clazz = WATCHER_TYPES.get(type);
		if(clazz != null)
			try {
				watcher = (Watcher)clazz.newInstance();
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		return watcher;
	}
}
