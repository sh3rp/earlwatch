package org.skendall.earlwatch.dir;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.skendall.earlwatch.Transition;
import org.skendall.earlwatch.WatchListener;
import org.skendall.earlwatch.Watcher;

public class FileWatcher implements Watcher {

	private Logger logger = Logger.getLogger(FileWatcher.class.getName());
	
	private Map<String,List<WatchListener>> listeners;
	
	private Map<String,List<Transition>> transitions;
		
	private ThreadedWatcher fileWatcher;
			
	public FileWatcher() {
		this.listeners = new HashMap<String,List<WatchListener>>();
		this.transitions = new HashMap<String,List<Transition>>();
		this.fileWatcher = new ThreadedWatcher(listeners,transitions);
		startThreads();
	}
	
	private void startThreads() {
		if(logger.isDebugEnabled())
			logger.debug("Starting watcher threads.");
		new Thread(fileWatcher).start();
	}
	
	public void addWatchListener(String url, WatchListener listener) {
		List<WatchListener> watchers = null;
		File[] files = getFilesToListenTo(url);
		for(File f : files) {
			String filename = f.getAbsolutePath();
			if(logger.isDebugEnabled())
				logger.debug("Adding watch listener: " + listener + " for URL " + filename);
			if(!listeners.containsKey(filename)) {
				if(logger.isDebugEnabled())
					logger.debug("No watch listeners found for URL: " + filename + " (creating)");
				watchers = new ArrayList<WatchListener>();
				listeners.put(filename, watchers);
			} else {
				if(logger.isDebugEnabled())
					logger.debug("Found watch listeners for URL: " + filename);
				watchers = listeners.get(filename);
			}
			watchers.add(listener);
			if(logger.isDebugEnabled())
				logger.debug("Added listener " + listener + " for URL " + filename);
		}
	}

	public void removeWatchListener(String url, WatchListener listener) {
		// TODO Auto-generated method stub
	}

	public void removeAllListeners(String url) {
		listeners.clear();
	}
	
	public String toString() {
		return fileWatcher.toString();
	}
	
	private File[] getFilesToListenTo(String url) {
		File[] files = null;
		
		File f = new File(url);
		if(!f.exists()) {
			files = new File[0];
		} else {
			if(f.isDirectory()) {
				files = f.listFiles();
			} else {
				files = new File[1];
				files[0] = f;
			}	
		}
		
		return files;
	}
	
    private class ThreadedWatcher implements Runnable {
		
    	private Logger logger = Logger.getLogger(ThreadedWatcher.class.getName());
    	
		private Set<String> urls;
		private Map<String,List<WatchListener>> listeners;
		private Map<String,List<Transition>> transitions;
		
		private boolean captureDiffs = false;
		
		private boolean done;
		
		public ThreadedWatcher(Map<String,List<WatchListener>> listeners, Map<String,List<Transition>> transitions) {
			this.listeners = listeners;
			this.urls = listeners.keySet();
			this.transitions = transitions;
		}
		
		public void run() {
			if(logger.isDebugEnabled())
				logger.debug("Starting Thread watcher.");
			while(!done) {
				for(String filename : urls) {
					File file = new File(filename);
					//if(logger.isDebugEnabled())
						//logger.debug("Checking " + file.getAbsolutePath());
					checkFileChange(file);
					if(file.isDirectory()) {
						File[] fileList = file.listFiles();
						for(File f : fileList)
							checkFileChange(f);
					} 
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		public void stop() {
			done = true;
		}
		
		public void reset() {
			done = false;
		}
		
		public boolean isCaptureDiffs() {
			return captureDiffs;
		}

		public void setCaptureDiffs(boolean captureDiffs) {
			this.captureDiffs = captureDiffs;
		}

		private Transition getLastTransition(File file) {
			Transition last = null;
			if(!transitions.containsKey(file.getAbsolutePath())) {
				transitions.put(file.getAbsolutePath(), new ArrayList<Transition>());
				last = new Transition();
				last.setLastModified(file.lastModified());
				transitions.get(file.getAbsolutePath()).add(last);
			} else {
				last = transitions.get(file.getAbsolutePath()).get(transitions.get(file.getAbsolutePath()).size()-1);
			}
			return last;
		}
		
		private Transition newTransition(File file) {
			Transition t = new Transition();
			t.setLastModified(file.lastModified());
			t.setUrl(file.getAbsolutePath());
			return t;
		}
		
		private void checkFileChange(File file) {
			if(getLastTransition(file).getLastModified() < file.lastModified()) {
				Transition t = newTransition(file);
				if(logger.isDebugEnabled())
					logger.debug("Created new transition: " + t);
				transitions.get(file.getAbsolutePath()).add(t);
				if(logger.isDebugEnabled())
					logger.debug("Publishing transition to listeners.");
				if(listeners.containsKey(file.getAbsolutePath()))
					for(WatchListener listener : listeners.get(file.getAbsolutePath()))
						listener.changed(new FileEvent(t));
			}
		}
		
		public String toString() {
			return "[ThreadedWatcher]";
		}
	
	}

	public List<Transition> getTransitions(String url) {
		
		List<Transition> t = new ArrayList<Transition>();
		
		File f = new File(url);
		String myUrl = f.getAbsolutePath();
		if(transitions.containsKey(myUrl))
			t = transitions.get(myUrl);
		return t;
	}

}
