package org.skendall.earlwatch.dir;

import org.skendall.earlwatch.Transition;
import org.skendall.earlwatch.WatcherEvent;
import org.skendall.earlwatch.WatcherStat;

public class FileEvent implements WatcherEvent {

	private Transition transition;
	
	public FileEvent(Transition t) {
		this.transition = t;
	}
	
	public Object getSource() {
		// TODO Auto-generated method stub
		return transition;
	}

	public WatcherStat getStat() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getWatchedUrl() {
		// TODO Auto-generated method stub
		return transition.getUrl();
	}

	public String toString() {
		return "[FileEvent] url=" + transition.getUrl();
	}

}
