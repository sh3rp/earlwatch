package org.skendall.earlwatch;

/**
 * 
 * @author skendall
 *
 */

public interface WatcherEvent {
	
	/**
	 * Depending on the type of Watcher Event, this method will return
	 * the type of event.  At the moment only a Transition object is
	 * supported.
	 * 
	 * This should eventually become a WatcherEventSource interface.
	 * 
	 * @return Object
	 */
	
	public Object getSource();
	
	/**
	 * Not currently implemented.  A WatcherStat will be an object that
	 * provides an API to collect metrics on transitions and other
	 * watcher events.
	 * 
	 * @return WatcherStat
	 */
	
	public WatcherStat getStat();
	
	/**
	 * Returns the URL that triggered the WatcherEvent.
	 * 
	 * @return String
	 */
	
	public String getWatchedUrl();

}
