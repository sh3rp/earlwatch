package org.skendall.earlwatch;

import java.util.List;

/**
 * Not yet implemented.
 * 
 * @author skendall
 *
 */

public class WatcherStat {
	
	/**
	 * 
	 */
	
	private long noticedOn;
	
	/**
	 * 
	 */
	
	private List<Transition> transitions;
	
	public long getNoticedOn() {
		return noticedOn;
	}
	
	public void setNoticedOn(long noticedOn) {
		this.noticedOn = noticedOn;
	}
	
	public List<Transition> getTransitions() {
		return transitions;
	}
	
	public void setTransitions(List<Transition> transitions) {
		this.transitions = transitions;
	}
	
}
